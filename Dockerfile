FROM nginx:alpine


RUN rm /etc/nginx/conf.d/default.conf

COPY nginx.conf.template /etc/nginx/conf.d/nginx.conf.template
COPY start-nginx.sh /start-nginx.sh


COPY . /usr/share/nginx/html


RUN chmod +x /start-nginx.sh

CMD ["/start-nginx.sh"]
