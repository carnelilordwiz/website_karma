pipeline {
    agent any

    environment {
        DOCKER_IMAGE = 'nelzer95/website_karma'
        IMAGE_TAG = 'latest'
        REGISTRY_CREDENTIALS_ID = 'dockerhub-login'
        HEROKU_APP_NAME = 'websitekarma4'
        HEROKU_API_KEY_ID = 'heroku-api-key'
    }

    stages {
        stage('Build image') {
            steps {
                script {
                    echo "Building Docker image..."
                    sh "docker build -t ${DOCKER_IMAGE}:${IMAGE_TAG} ."
                }
            }
        }

        stage('Push image to Registry') {
            steps {
                script {
                    echo "Logging into Docker Hub and pushing image..."
                    withCredentials([usernamePassword(credentialsId: REGISTRY_CREDENTIALS_ID, usernameVariable: 'REGISTRY_USER', passwordVariable: 'REGISTRY_PASS')]) {
                        sh "docker login --username $REGISTRY_USER --password $REGISTRY_PASS"
                        sh "docker push ${DOCKER_IMAGE}:${IMAGE_TAG}"
                    }
                }
            }
        }

        stage('Deploy to Heroku') {
            steps {
                script {
                    echo "Deploying to Heroku..."
                    withCredentials([string(credentialsId: HEROKU_API_KEY_ID, variable: 'HEROKU_API_KEY')]) {
                        sh "docker tag ${DOCKER_IMAGE}:${IMAGE_TAG} registry.heroku.com/${HEROKU_APP_NAME}/web"
                        sh "docker login --username=_ --password $HEROKU_API_KEY registry.heroku.com"
                        sh "docker push registry.heroku.com/${HEROKU_APP_NAME}/web"
                        sh "heroku container:release web --app ${HEROKU_APP_NAME}"
                    }
                }
            }
        }


        stage('Check Deployment') {
            steps {
                script {
                    int attempts = 0
                    while (attempts < 10) {
                        try {
                            echo "Checking deployment..."
                            sh "curl -s https://websitekarma4-d701058279c2.herokuapp.com | grep 'Free Shipping on all order'"
                            break
                        } catch (Exception e) {
                            attempts++
                            if (attempts >= 10) {
                                echo "Deployment check failed after multiple attempts."
                                throw new Exception("Deployment content check failed.")
                            }
                            echo "Waiting for deployment to be ready..."
                            sleep(time: 30, unit: 'SECONDS')
                        }
                    }
                }
            }
        }
    }
    
    post {
        success {
            echo "Sending build success email..."
            emailext(
                to: 'carnelilordwiz@gmail.com',
                subject: "Build ${currentBuild.fullDisplayName} completed successfully",
                body: """<html><body>
                         <p>The build of <strong>${env.JOB_NAME}</strong> build number <strong>${env.BUILD_NUMBER}</strong> was completed successfully.</p>
                         <p>Status: <strong>${currentBuild.currentResult}</strong></p>
                         <p>View build details at <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                         </body></html>""",
                mimeType: 'text/html'
            )
        }
        failure {
            echo "Sending build failure email..."
            emailext(
                to: 'carnelilordwiz@gmail.com',
                subject: "Build ${currentBuild.fullDisplayName} failed",
                body: """<html><body>
                         <p>The build of <strong>${env.JOB_NAME}</strong> build number <strong>${env.BUILD_NUMBER}</strong> failed.</p>
                         <p>Status: <strong>${currentBuild.currentResult}</strong></p>
                         <p>View build details at <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                         </body></html>""",
                mimeType: 'text/html'
            )
        }
    }
}
